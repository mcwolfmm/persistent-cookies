const {Cu} = require("chrome");
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/FileUtils.jsm");

var database = {
	conn: null,
	
	init: function() {
		var file = FileUtils.getFile("ProfD", ["cookies.sqlite"]);
		
		this.conn = Services.storage.openDatabase(file);
		this.conn.executeSimpleSQL("create table if not exists moz_cookies_persistent (baseDomain text unique)");
	},
	
	isExists: function(baseDomain) {
		var statement = this.conn.createStatement("select count(*) as c from moz_cookies_persistent where baseDomain = :baseDomain");
		statement.params.baseDomain = baseDomain;
		
		if(statement.step()) {
			return statement.row.c === 0;
		}
		
		return false;
	},
	
	insert: function(baseDomain) {
		console.log("isnert " + baseDomain);
				
		var statement = this.conn.createStatement("insert into moz_cookies_persistent (baseDomain) values ( :baseDomain )");
		statement.params.baseDomain = baseDomain;
		statement.execute();
	},
	
	delete: function(baseDomain) {
		console.log("delete " + baseDomain);
				
		var statement = this.conn.createStatement("delete from moz_cookies_persistent where baseDomain = :baseDomain");
		statement.params.baseDomain = baseDomain;
		statement.execute();
	},
	
	deleteCookies: function() {
		this.conn.executeSimpleSQL("delete from moz_cookies where baseDomain not in (select baseDomain from moz_cookies_persistent)");
	}
}

database.init();

var browser = require("sdk/windows").browserWindows;
browser.on('close', function(window) {
	database.deleteCookies();
});

var self = require("sdk/self");
var contextMenu = require("sdk/context-menu");
var menuItem = contextMenu.Item({
	label: "Keep cookies",	
	contentScriptFile: self.data.url('menu.js'),
		
	onMessage: function(baseDomain) {
		if(database.isExists(baseDomain)) {
			database.insert(baseDomain);
		}
		else {
			database.delete(baseDomain);
		}
	}
});